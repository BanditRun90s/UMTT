# Over one year after I've noticed a one line of translation that enough to broke the trust of one famous Thai translator for me, and still no one are do something better, include any better actions from himself...

### What's a story...?
I've manually found by myself that I think the Thai translation are hide political message in the one line map stash, where you can read that string when you've found annotated map that point to hunting lounge building in the forest near trainline of _West Point_. (Stash no.15)
Here's a comparision between language...

#### English
```lua
Stash_EN = {
...
    Stash_WpMap15_Text1 = "They'll never find us here, trust me",
    Stash_WpMap15_Text11 = "they're dumber than dog shit",
...
}
```

#### Thai ([blame](https://github.com/rikoprushka/ProjectZomboidTranslations/blame/7f4fb63f626cd527e15c755b1e76f866aef4e63d/TH/Stash_TH.txt#L180))
```lua
Stash_TH = {
...
    Stash_WpMap15_Text1 = "พวกมันหาเราไม่เจอหรอก เชื่อดิ",
    Stash_WpMap15_Text11 = "พวกมันโง่ยิ่งกว่าปลาหยุดอีก",
...
}
```

I'll trying to compare between words by words...

| Words Table | 1 | 2 | 3 | 4 | 5 |
| ----------- | - | - | - | - | - |
| English     | they're | dumber | than | dog | shit |
| Thai        | พวกมัน | โง่ | ยิ่งกว่า | ปลา | หยุด |
| 

What's the real match of dog shit for Thai?
- ปลา = Fish != Dog
- หยุด = Stop != Shit

> If you're spoke fast, it's trying to indirectly pun to spell `Playood`, that's mean to **`Prayuth`**, a former prime minister in Thai that got defame often in my region with mostly false and rumored news, so...

If he's a good translator, why he still keeping that string too long since 2022 until I've tell him to change, and why none of any Thai players found this and tell to him or any of TIS developers?

After that, are he doing any better actions just to make pull requests to change problematic phrase, why he didn't also tell in commit that he was added political message in translation and need to merge to fix that as fast as possible.

As long as Build 42 didn't arriving, those phrase will still remain, unless you've subscribed to Thai translation mod that was fixed later or manually replace translated stash file in base game directory by yourself!

**Afterwords, are none of Thai did even care to little things about this, or just me!?**

-----

### Second, I won't join `Discord` servers!
I think I will tell the above problems on the `Discord` servers, too, however.

**It seems like official server of it aren't allow users who didn't verified identity with phone numbers to at least have basic interactions to type-in chat messages, so I abandoned that plan!**

Even just translation repository, he urged contributors who make pull requests about Thai translation to join said servers for discussion, to ensure that the translations are going well on the same ways and same goals!

In my opinion...

#### IF `DISCORD` IS ONE AND ONLY WAYS TO COMMUNICATE WITH DEVELOPERS, CONTRIBUTORS, TRANSLATORS, ETC. I WILL STRONGLY NOT JOIN IT AT ALL COSTS, PERIOD!