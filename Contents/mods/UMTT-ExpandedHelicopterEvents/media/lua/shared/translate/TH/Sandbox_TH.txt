Sandbox_TH = {
    Sandbox_ExpandedHeli = "กิจกรรมทางอากาศ (ส่วนขยาย)",

    Sandbox_ExpandedHeli_Frequency_option1 = "ไม่มี"
    Sandbox_ExpandedHeli_Frequency_option2 = "นานๆ ครั้ง",
    Sandbox_ExpandedHeli_Frequency_option3 = "ไม่ค่อยบ่อย",
    Sandbox_ExpandedHeli_Frequency_option4 = "ปกติ",
    Sandbox_ExpandedHeli_Frequency_option5 = "บ่อย",
    Sandbox_ExpandedHeli_Frequency_option6 = "บ่อยมาก",

    Sandbox_ExpandedHeli_AirRaidSirenEvent = "เสียงเตือนการโจมตีทางอากาศ",
    Sandbox_ExpandedHeli_AirRaidSirenEvent_tooltip = "เปิดตัวเลือกนี้หากต้องการท้าทายตัวเกมในระดับยากโดยการเร่งความรุนแรงของการโจมตีทางอากาศให้เร็วขึ้นและยากขึ้นมากในช่วงเลทเกม โดยเฉพาะการทิ้งระเบิด",

    Sandbox_ExpandedHeli_EventJet = "เครื่องบินเจ็ท",
    Sandbox_ExpandedHeli_EventJet_tooltip = "ความถี่ที่เครื่องบินเจ็ทจะบินผ่านผู้เล่น<br>กิจกรรมนี้จะทำให้เกิดการเคลื่อนตัวของกลุ่มซอมบี้",

    Sandbox_ExpandedHeli_EventNews = "ช่องข่าว",
    Sandbox_ExpandedHeli_EventNews_tooltip = "ความถี่ที่เฮลิคอปเตอร์สื่อมวลชนจะรบกวนผู้เล่น<br>กิจกรรมนี้จะมีเฮลิคอปเตอร์ค้นหาผู้เล่นในเส้นทางบิน และจะติดตามผู้เล่นที่พบเจอในช่วงระยะเวลาหนึ่งก่อนจะบินต่อไป",

    Sandbox_ExpandedHeli_EventPolice = "ตำรวจ",
    Sandbox_ExpandedHeli_EventPolice_tooltip = "ความถี่ที่เฮลิคอปเตอร์ตำรวจจะช่วยเหลือผู้เล่น<br>กิจกรรมนี้จะมีเฮลิคอปเตอร์ช่วยเหลือผู้เล่นและกำจัดซอมบี้ที่อยู่ใกล้เคียง",

    Sandbox_ExpandedHeli_EventMilitary = "ทหาร",
    Sandbox_ExpandedHeli_EventMilitary_tooltip = "ความถี่ที่เฮลิคอปเตอร์ทหารจะรบกวนผู้เล่น<br>กิจกรรมนี้จะมีลำดับความรุนแรง เริ่มจาก...<br>1.การแจ้งเตือนให้อยู่ในที่กำบัง<br>2.กำจัดซอมบี้โดยรอบบริเวณที่บินผ่าน และ<br>3.กำจัดทุกสิ่งมีชีวิตที่เคลื่อนไหว ในที่นี้รวมถึงตัวผู้เล่นด้วย",

    Sandbox_ExpandedHeli_EventFEMA = "สาธารณสุข FEMA",
    Sandbox_ExpandedHeli_EventFEMA_tooltip = "ความถี่ที่เฮลิคอปเตอร์สาธารณสุขจะช่วยเหลือผู้เล่น<br>กิจกรรมนี้จะมีเฮลิคอปเตอร์ FEMA จัดส่งอุปกรณ์ปฐมพยาบาลระหว่างการแพร่ระบาด",

    Sandbox_ExpandedHeli_EventSurvivor = "ผู้รอดชีวิต",
    Sandbox_ExpandedHeli_EventSurvivor_tooltip = "ความถี่ที่เฮลิคอปเตอร์ผู้รอดชีวิตจะรบกวนผู้เล่น<br>กิจกรรมนี้จะมีเฮลิคอปเตอร์บินผ่านเพื่อมองหาสิ่งต่างๆ รวมถึงตัวผู้เล่น และทำให้เกิดการเดินทางของกลุ่มซอมบี้",

    Sandbox_ExpandedHeli_EventRaider = "โจร",
    Sandbox_ExpandedHeli_EventRaider_tooltip = "ความถี่ที่เฮลิคอปเตอร์โจรจะรบกวนผู้เล่น<br>กิจกรรมนี้จะมีเฮลิคอปเตอร์โจรยิงกราดตามเส้นทางที่บินเพื่อสนองความบันเทิงส่วนตัวของกลุ่มโจร",

    Sandbox_ExpandedHeli_EventSamaritan = "ผู้ช่วย",
    Sandbox_ExpandedHeli_EventSamaritan_tooltip = "ความถี่ที่เฮลิคอปเตอร์ผู้รอดชีวิตจะช่วยเหลือผู้เล่น<br>กิจกรรมนี้จะมีกลุ่มผู้รอดชีวิตอื่นจัดส่งชุดอุปกรณ์ยังชีพเพื่อช่วยให้ผู้เล่นอยู่รอดได้อีกหลายวัน",

    Sandbox_ExpandedHeli_StartDay = "วันที่เริ่ม",
    Sandbox_ExpandedHeli_StartDay_tooltip = "ตั้งค่าวันที่กิจกรรมทางอากาศเริ่มเปิดใช้งาน แนะนำให้ตั้งค่าที่แปรผันกับ "จำนวนเดือนหลังจากการระบาด"",

    Sandbox_ExpandedHeli_SchedulerDuration = "ระยะเวลาของกำหนดการ",
    Sandbox_ExpandedHeli_SchedulerDuration_tooltip = "ระบุระยะเวลาของกิจกรรมทางอากาศ (หน่วยเป็นวัน) กำหนดการต่างๆ ของแต่ละกิจกรรมทางอากาศที่เปิดใช้จะถูกกำหนดช่วงเวลาให้เหมาะสมตามค่าที่ระบุ",

    Sandbox_ExpandedHeli_ContinueScheduling = "วนเหตุการณ์ตามกำหนดการอย่างต่อเนื่อง",
    Sandbox_ExpandedHeli_ContinueScheduling_tooltip = "หากเปิดตัวเลือกนี้จะวนใช้เหตุการณ์ตามกำหนดอย่างต่อเนื่องหลังจากวันที่ครบกำหนดด้านบน แต่ระดับของเหตุการณ์ทางอากาศจะไม่เปลี่ยนแปลงและไม่สามารถหยุดกำหนดการณ์หลังจากนั้นได้<br>ตัวอย่างเช่น หากเฮลิคอปเตอร์ทหารมีความรุนแรงที่ระดับสูงสุด (ระดับ 3) จะไม่มีการย้อนกลับไปที่ระดับ 1 อีกเมื่อเข้าสู่ช่วงวนเหตุการณ์",

    Sandbox_ExpandedHeli_ContinueSchedulingLateGameOnly = "เฉพาะเหตุการณ์ช่วงเลทเกมเท่านั้น",
    Sandbox_ExpandedHeli_ContinueSchedulingLateGameOnly_tooltip = "หากเปิดตัวเลือกนี้เมื่อตัวเลือก "วนเหตุการณ์ตามกำหนดการอย่างต่อเนื่อง" เปิดอยู่ จะวนใช้เหตุการณ์เฉพาะที่เหมาะสมกับช่วงที่ผ่านการแพร่ระบาดมานานแล้ว",

    Sandbox_ExpandedHeli_WeatherImpactsEvents = "สภาพอากาศมีผลต่อการบิน",
    Sandbox_ExpandedHeli_WeatherImpactsEvents_tooltip = "หากเปิดตัวเลือกนี้ สภาพอากาศจะเป็นปัจจัยที่ส่งผลต่อกิจกรรมทางอากาศ อาจทำให้ไม่มีเหตุการณ์ของเครื่องบิน หรือทำให้เครื่องบินตก",

    Sandbox_ExpandedHeli_CrashChanceMulti = "โอกาสที่เครื่องบินตก",
    Sandbox_ExpandedHeli_CrashChanceMulti_tooltip = "ตัวคูณโอกาสที่เครื่องบินจะตก หากตั้งค่าเป็น 0 จะปิดใช้งาน",
}
