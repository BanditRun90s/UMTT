DynamicRadio_TH = {
    AEBS_Water_Warning1 = "ระบบประปาน็อกซ์: ตรวจพบความผันผวนของแรงดันน้ำ"
    AEBS_Water_Warning2 = "ระบบประปาน็อกซ์: ระบบล้มเหลว แรงดันน้ำลดต่ำลง"
    AEBS_Water_Warning3 = "ระบบประปาน็อกซ์: น้ำแล้ง"
}