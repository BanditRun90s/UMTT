Sandbox_TH = {
	Sandbox_TomaroImmunity = "ระบบภูมิคุ้มกัน",

	Sandbox_TomaroImmunity_ImmuneChance = "โอกาสที่จะมีภูมิคุ้มกัน",
	Sandbox_TomaroImmunity_ImmuneChance_tooltip = "เปอร์เซ็นในการที่ผู้เล่นมีโอกาสที่จะได้รับภูมิคุ้มกันหลังจากเกิดใหม่",

	Sandbox_TomaroImmunity_WaitPeriod = "การรอตรวจเช็ค",
	Sandbox_TomaroImmunity_WaitPeriod_tooltip = "เวลาในการรอเพื่อที่จะตรวจรอยกัดในแต่ละครั้ง",
}