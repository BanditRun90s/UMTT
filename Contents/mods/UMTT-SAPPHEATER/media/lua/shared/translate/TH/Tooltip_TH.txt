ContextMenu_EN = {
    Tooltip_SmallHeater = "เครื่องทำความร้อนขนาดเล็กที่ใช้พลังงานจากแบตเตอรี่รถยนต์",
    Tooltip_OutdoorHeater = "เครื่องทำความร้อนภายนอกอาคารที่ใช้พลังงานจากแบตเตอรี่รถยนต์",
    Tooltip_ElectricHeater = "เครื่องทำความร้อนที่ใช้พลังงานจากไฟฟ้า หรือเครื่องกำเนิดไฟฟ้า",
    Tooltip_HeaterCircuit = "เป็นอุปกรณ์สำหรับประดิษฐ์เครื่องทำความร้อน",
    Tooltip_ElectriciansIssue1 = "เรียนรู้วิธีการสร้างเครื่องทำความร้อนขนาดเล็ก",
    Tooltip_ElectriciansIssue2 = "เรียนรู้วิธีการสร้างเครื่องทำความร้อนภายนอกอาคาร",
    Tooltip_ElectriciansIssue3 = "เรียนรู้วิธีการสร้างเครื่องทำความร้อนพลังงานไฟฟ้า",
}