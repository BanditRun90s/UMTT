Sandbox_TH = {
	Sandbox_PZNS_Framework = "เฟรมเวิร์ก PZNS",
	
	Sandbox_PZNS_Framework_IsDebugModeActive = "เปิดใช้งานโหมดดีบัก",
	Sandbox_PZNS_Framework_IsDebugModeActive_tooltip = "ติ๊กถูกเพื่อเพื่อเปิดใช้งาน หรือยกเลิกการเลือกเพื่อปิดการใช้งาน",

	Sandbox_PZNS_Framework_IsInfiniteAmmoActive = "NPC มีกระสุนไม่จำกัด?",
	Sandbox_PZNS_Framework_IsInfiniteAmmoActive_tooltip = "ติ๊กถูกเพื่อเพื่อเปิดใช้งาน หรือยกเลิกการเลือกเพื่อปิดการใช้งาน<br>NPCs จะทำการยิงปืนอย่างต่อเนื่องต่อไปโดยไม่สนใจจำนวนกระสุนที่มีอยู่ในตัว",

	Sandbox_PZNS_Framework_IsNPCsNeedsActive = "(กำลังทดสอบ) NPC มีความต้องการ",
	Sandbox_PZNS_Framework_IsNPCsNeedsActive_tooltip = "(กำลังทดสอบ) ติ๊กถูกเพื่อเพื่อเปิดใช้งาน หรือยกเลิกการเลือกเพื่อปิดการใช้งาน<br>NPC มีความต้องการอาหารและน้ำหากเปิดใช้งาน",

	Sandbox_PZNS_Framework_GroupSizeLimit = "(กำลังทดสอบ) ขีดจำกัดของขนาดกลุ่ม",
	Sandbox_PZNS_Framework_GroupSizeLimit_tooltip = "(กำลังทดสอบ) ตั้งค่าขีดจำกัด ของ NPC ที่กลุ่มอาจมี",

	Sandbox_PZNS_Framework_CompanionFollowRange = "ระยะห่างในการเดินตาม",
	Sandbox_PZNS_Framework_CompanionFollowRange_tooltip = "กำหนดระยะห่างที่ NPC จะพยายามติดตามผู้เล่น",

	Sandbox_PZNS_Framework_CompanionRunRange = "ระยะห่างในการวิ่งตาม",
	Sandbox_PZNS_Framework_CompanionRunRange_tooltip = "ตั้งค่าระยะห่างที่ NPC จะเริ่มวิ่งตามผู้เล่นให้ทัน",

	Sandbox_PZNS_Framework_CompanionIdleTicks = "(กำลังทดสอบ) หน่วงเวลาของ NPC ที่เป็นสหายก่อนหยุดนิ่ง",
	Sandbox_PZNS_Framework_CompanionIdleTicks_tooltip = "(กำลังทดสอบ) ตั้งค่าหน่วงเวลา (Tick) ที่ NPC จะเริ่มเข้าสู่โหมดหยุดนิ่ง (Idle)",


}
