Recipes_TH = {

/*Actions*/
	Recipe_Saw_Off_Shotgun_Stock = "ตัดพานท้ายปืนลูกซองออก",
	Recipe_Saw_Off_Double_Barrel_Shotgun_Stock = "ตัดพานท้ายปืนลูกซองออก",

	Recipe_Open_Box_of_7.62_Ammo = "เปิดกล่องกระสุนปืน 7.62 มม.",
	Recipe_Place_7.62_Ammo_in_Box = "บรรจุกระสุนปืน 7.62 มม. ลงกล่อง",

	Recipe_Open_Box of_.22_Ammo = "เปิดกล่องกระสุนปืน .22",
	Recipe_Place_.22_Ammo_in_Box = "บรรจุกระสุนปืน .22 ลงกล่อง",

	Recipe_Couple_5.56_Magazines = "ทำแมกกาซีนมัดคู่ M16",
	Recipe_Separate_5.56_Magazines = "แยกแมกกาซีนมัดคู่ M16",

	Recipe_Couple_7.62_Magazines = "ทำแมกกาซีนมัดคู่ AK47",
	Recipe_Separate_7.62_Magazines = "แยกแมกกาซีนมัดคู่ AK47",

	Recipe_Attach_M9_Bayonet_to_Spear = "ติดมีดปลายปืนกับหอก",
	Recipe_Reclaim_M9_Bayonet_from_Spear = "ถอดมีดปลายปืนออกจากหอก",
}