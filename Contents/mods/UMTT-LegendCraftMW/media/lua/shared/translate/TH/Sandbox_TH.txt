Sandbox_TH = {	
	Sandbox_LegendCraft = "ตีบวกอาวุธขั้นเทพ",
	Sandbox_LCMWCustDamage = "ตัวคูณดาเมจอาวุธระยะประชิด (ขั้นที่ ๒-๕)",
	Sandbox_LCMWCustDamage_tooltip = "เพิ่มความแรงในสัดส่วนร้อยละ ต่อการตีบวกในแต่ละขั้น เริ่มตั้งแต่ ๒ ถึง ๕"
	Sandbox_LCMWLegDamage = "ตัวคูณดาเมจอาวุธระยะประชิด (เฉพาะขั้นเทพ)",
	Sandbox_LCMWLegDamage_tooltip = "คูณดาเมจให้อาวุธขั้นเทพเมื่อเทียบกับดาเมจของอาวุธชิ้นนั้นว่าจะแรงกว่ากี่เท่าของอาวุธปกติที่ไม่ได้ตีบวกเลย"
	Sandbox_LCMWDisableLegend = "ให้อาวุธขั้นเทพมีโอกาสพัง",
	Sandbox_LCMWDisableLegend_tooltip = "เปลี่ยนค่าความคงทนของอาวุธขั้นเทพให้มีโอกาสที่จะพังลงได้"
}