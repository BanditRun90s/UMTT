Tooltip_TH = {
	Tooltip_EggonsDoors_DoorAllOrientations = "This door can be installed in any orientation",
	Tooltip_EggonsDoors_InstallOrientation = "This door can be installed only in its current orientation - facing",
	Tooltip_EggonsDoors_Unlocked = "ปลดล็อคประตูแล้ว",
	Tooltip_EggonsDoors_North = "ทิศเหนือ",
	Tooltip_EggonsDoors_West = "ทิศตะวันออก",
	Tooltip_EggonsDoors_Door = "ประตู",
	Tooltip_EggonsDoors_InstallDescription = "Install the selected door anywhere you want. It is required to place it in a Door Frame.",
	Tooltip_EggonsDoors_RemoveDescription = "Take off the hinges from the current selected door, allowing to install it somewhere else. <LINE> (Double doors don't work)",
}