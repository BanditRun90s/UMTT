IG_UI_TH = {
	IGUI_WPCmd_Water = "ประปา",
	IGUI_WPCmd_Power = "ไฟฟ้า",

	IGUI_WPCmd_TurnedOn = "สถานะเปลี่ยนเป็น "เปิด" แล้ว",
	IGUI_WPCmd_TurnedOff = "สถานะเปลี่ยนเป็น "ปิด" แล้ว",

	IGUI_WPCmd_On = "เปิด",
	IGUI_WPCmd_Off = "ปิด",
	IGUI_WPCmd_ModName = "ควบคุมไฟฟ้าและประปา",
	IGUI_WPCmd_Toggle = "สลับสถานะไฟฟ้า/ประปา",
}