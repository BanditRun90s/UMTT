ContextMenu_TH = {
    ContextMenu_Pick_Up_Bicycle = "ยกจักรยาน",
    ContextMenu_BicycleLock = "ติดตั้งกุญแจล็อค",
    ContextMenu_BicycleUnlock = "เอากุญแจล็อคออก",
    ContextMenu_Lift_Bicycle = "ตั้งจักรยานขึ้น",
}