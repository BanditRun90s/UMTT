Recipes_TH = {
    Recipe_CraftScrapFrame1 = "เชื่อมโครงจักรยาน (เก่า)",
    Recipe_CraftScrapSuspension1 = "เชื่อมตะเกียบจักรยาน (เก่า)",
    Recipe_CraftScrapBrakes1 = "เชื่อมเบรคจักรยาน (เก่า)",
    Recipe_CraftScrapTire1 = "ทำล้อจักรยาน (เก่า)",

    Recipe_CraftScrapFrame2 = "เชื่อมโครงจักรยานเสือภูเขา (เก่า)",
    Recipe_CraftScrapSuspension2 = "เชื่อมตะเกียบจักรยานเสือภูเขา (เก่า)",
    Recipe_CraftScrapBrakes2 = "เชื่อมเบรคจักรยานเสือภูเขา (เก่า)",
    Recipe_CraftScrapTire2 = "ทำล้อจักรยานเสือภูเขา (เก่า)",

    Recipe_CraftScrapSeat = "ทำเบาะจักรยาน (เก่า)",
    Recipe_CraftBikeLock = "ทำแม่กุญแจล็อคจักรยาน",
    Recipe_CraftHeadlight = "ทำไฟหน้าจักรยาน",
}