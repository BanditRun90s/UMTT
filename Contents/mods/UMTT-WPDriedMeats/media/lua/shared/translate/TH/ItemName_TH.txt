ItemName_TH {
  ItemName_Base.WPDriedMeatCan = "กระป๋องเนื้ออบแห้ง",

  ItemName_Base.WPDriedChicken = "เนื้อไก่แช่แข็ง (แห้ง)",
  ItemName_Base.WPDriedPorkChop = "เนื้อหมูแช่แข็ง (แห้ง)",
  ItemName_Base.WPDriedSteak = "เนื้อสเต็กแช่แข็ง (แห้ง)",
}